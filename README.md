# Kawal Diri Tracker Service
**See overall Kawal Diri engineering architecture [here](https://gitlab.com/kawalcovid19/kawal-diri/docs)**

## Tracker Service Architecture
![](docs/architecture.png)

### Graph Design
![](docs/graph_design.png)