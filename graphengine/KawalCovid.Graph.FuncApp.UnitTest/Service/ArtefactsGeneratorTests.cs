﻿using Xunit;
using KawalCovid.Graph.FuncApp.Service;
using System;
using System.Collections.Generic;
using System.Text;
using KawalCovid.Graph.FuncApp.Model;
using Newtonsoft.Json;

namespace KawalCovid.Graph.FuncApp.Service.Tests
{
    public class ArtefactsGeneratorTests
    {
        [Fact()]
        public void GenerateClusterArtefacts()
        {
            Cluster cluster = new Cluster();
            cluster.ID = "clu-1";
            cluster.CreatedDate = DateTime.Now;
            cluster.City = "Jakarta";
            cluster.Name = "Cluster Name";
            cluster.Province = "DKI Jakarta";

            string artefact = JsonConvert.SerializeObject(cluster);

            System.IO.File.WriteAllText(@"output\cluster.json", artefact);
        }

        [Fact()]
        public void GenerateEventArtefacts()
        {
            Event eventInstance = new Event();
            eventInstance.ID = "clu-1";
            eventInstance.CreatedDate = DateTime.Now;
            eventInstance.City = "Jakarta";
            eventInstance.Name = "Event Name";
            eventInstance.Province = "DKI Jakarta";
            eventInstance.Place = "Place Name";
            eventInstance.PlaceID = "Place ID";
            eventInstance.Tag = "tag1,tag2";

            string artefact = JsonConvert.SerializeObject(eventInstance);

            System.IO.File.WriteAllText(@"output\event.json", artefact);
        }

        [Fact()]
        public void GenerateHospitalArtefacts()
        {
            Hospital hospital = new Hospital();
            hospital.ID = "hos-1";
            hospital.CreatedDate = DateTime.Now;
            hospital.Name = "Hospital Name";

            string artefact = JsonConvert.SerializeObject(hospital);

            System.IO.File.WriteAllText(@"output\hospital.json", artefact);
        }

        [Fact()]
        public void GeneratePatientArtefacts()
        {
            Patient patient = new Patient();
            patient.ID = "pat-1";
            patient.CreatedDate = DateTime.Now;

            string artefact = JsonConvert.SerializeObject(patient);

            System.IO.File.WriteAllText(@"output\patient.json", artefact);
        }

        [Fact()]
        public void GeneratePersonArtefacts()
        {
            Person person = new Person();
            person.ID = "per-1";
            person.CreatedDate = DateTime.Now;

            string artefact = JsonConvert.SerializeObject(person);

            System.IO.File.WriteAllText(@"output\person.json", artefact);
        }

        [Fact()]
        public void GeneratePlaceArtefacts()
        {
            Place place = new Place();
            place.ID = "pla-1";
            place.CreatedDate = DateTime.Now;

            string artefact = JsonConvert.SerializeObject(place);

            System.IO.File.WriteAllText(@"output\place.json", artefact);
        }
    }
}