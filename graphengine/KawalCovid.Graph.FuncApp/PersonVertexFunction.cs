using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using KawalCovid.Graph.FuncApp.Model;
using KawalCovid.Graph.FuncApp.Service;

namespace KawalCovid.Graph.FuncApp
{
    public static class PersonVertexFunction
    {
        [FunctionName("PersonVertexFunction")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            try
            {
                string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
                Person data = JsonConvert.DeserializeObject<Person>(requestBody);

                PersonService svc = new PersonService();
                svc.GraphDataAccess = new DataAccess.GraphDataAccess(log);

                using (var gremlinClient = svc.GraphDataAccess.InstantiateGremlinClient())
                {
                    await svc.AddVertex(gremlinClient, data);
                }

                return (ActionResult)new OkObjectResult($"{data.ID}");
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Bad Request: " + e.Message);
            }
        }
    }
}
