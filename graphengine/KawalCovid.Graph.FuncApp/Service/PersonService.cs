﻿using KawalCovid.Graph.FuncApp.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace KawalCovid.Graph.FuncApp.Service
{
    public class PersonService : GraphServiceBase
    {
        public const string EDGE_PERSON_PERSON = "met_with";
        public const string EDGE_PERSON_PLACE = "visited";
        public const string EDGE_PERSON_EVENT = "attended";
        public const string EDGE_PERSON_PATIENT = "diagnosed_as";

        public override string VertexLabel => "person";

        protected override string GetAddVertexQuery(IGraphVertex vertex)
        {
            Person personVertex = vertex as Person;

            string queryAdd = "addV('" + VertexLabel + "').property('id', '" + personVertex.ID + "')";
            queryAdd += ".property('pk', '" + personVertex.ID + "')";
            queryAdd += ".property('age', " + personVertex.Age + ")";
            queryAdd += ".property('createdDate', '" + personVertex.CreatedDate + "')";
            queryAdd += ".property('nationality', '" + personVertex.Nationality + "')";
            queryAdd += ".property('sex', '" + personVertex.Sex + "')";

            return queryAdd;
        }

        public string AddEdgeToPerson(Person source, Person target, DateTime edgeTimeStamp, Dictionary<string, string> properties)
        {
            string edgeID = source.ID + "-" + target.ID + "-" + edgeTimeStamp.ToString("yyyyMMdd");

            string query = "g.V('" + source.ID + "').addE('" + EDGE_PERSON_PERSON + "').to(g.V('" + target.ID + "'))";
            query += ".property('id','" + edgeID + "')";
            query += ".property('createdDate','" + edgeTimeStamp + "')";

            foreach (var keyValuePair in properties)
            {
                query += ".property('" + keyValuePair.Key + "','" + keyValuePair.Value + "')";
            }

            return query;
        }

        public string AddEdgeToPlace(Person source, Place target, DateTime edgeTimeStamp, Dictionary<string, string> properties)
        {
            string edgeID = source.ID + "-" + target.ID + "-" + edgeTimeStamp.ToString("yyyyMMdd");

            string query = "g.V('" + source.ID + "').addE('" + EDGE_PERSON_PLACE + "').to(g.V('" + target.ID + "'))";
            query += ".property('id','" + edgeID + "')";
            query += ".property('createdDate','" + edgeTimeStamp + "')";

            foreach (var keyValuePair in properties)
            {
                query += ".property('" + keyValuePair.Key + "','" + keyValuePair.Value + "')";
            }

            return query;
        }

        public string AddEdgeToEvent(Person source, Event target, DateTime edgeTimeStamp, Dictionary<string, string> properties)
        {
            string edgeID = source.ID + "-" + target.ID + "-" + edgeTimeStamp.ToString("yyyyMMdd");

            string query = "g.V('" + source.ID + "').addE('" + EDGE_PERSON_EVENT + "').to(g.V('" + target.ID + "'))";
            query += ".property('id','" + edgeID + "')";
            query += ".property('createdDate','" + edgeTimeStamp + "')";

            foreach (var keyValuePair in properties)
            {
                query += ".property('" + keyValuePair.Key + "','" + keyValuePair.Value + "')";
            }

            return query;
        }

        public string AddEdgeToPatient(Person source, Patient target, DateTime edgeTimeStamp, Dictionary<string, string> properties)
        {
            string edgeID = source.ID + "-" + target.ID;

            string query = "g.V('" + source.ID + "').addE('" + EDGE_PERSON_PATIENT + "').to(g.V('" + target.ID + "'))";
            query += ".property('id','" + edgeID + "')";
            query += ".property('createdDate','" + edgeTimeStamp + "')";

            foreach (var keyValuePair in properties)
            {
                query += ".property('" + keyValuePair.Key + "','" + keyValuePair.Value + "')";
            }

            return query;
        }
    }
}
