﻿using KawalCovid.Graph.FuncApp.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace KawalCovid.Graph.FuncApp.Service
{
    public class PatientService : GraphServiceBase
    {
        public const string EDGE_PATIENT_HOSPITAL = "hospitalised_at";
        public const string EDGE_PATIENT_PATIENT = "related_to";


        public override string VertexLabel => "patient";

        protected override string GetAddVertexQuery(IGraphVertex vertex)
        {
            Patient patientVertex = vertex as Patient;

            string queryAdd = "addV('" + VertexLabel + "').property('id', '" + patientVertex.ID + "')";
            queryAdd += ".property('pk', '" + patientVertex.ID + "')";
            queryAdd += ".property('age', " + patientVertex.Age + ")";
            queryAdd += ".property('createdDate', '" + patientVertex.CreatedDate + "')";
            queryAdd += ".property('nationality', '" + patientVertex.Nationality + "')";
            queryAdd += ".property('sex', '" + patientVertex.Sex + "')";
            queryAdd += ".property('caseNumber', '" + patientVertex.CaseNumber + "')";
            queryAdd += ".property('caseStatus', '" + patientVertex.CaseStatus + "')";
            queryAdd += ".property('city', '" + patientVertex.City + "')";
            queryAdd += ".property('deceasedDate', '" + patientVertex.DeceasedDate + "')";
            queryAdd += ".property('diagnosedDate', '" + patientVertex.DiagnosedDate + "')";
            queryAdd += ".property('dischargeDate', '" + patientVertex.DischargeDate + "')";
            queryAdd += ".property('province', '" + patientVertex.Province + "')";
            queryAdd += ".property('updatedDate', '" + patientVertex.UpdatedDate + "')";

            return queryAdd;
        }

        public string AddEdgeToPatient(Patient source, Patient target, DateTime edgeTimeStamp, Dictionary<string, string> properties)
        {
            string edgeID = source.ID + "-" + target.ID;

            string query = "g.V('" + source.ID + "').addE('" + EDGE_PATIENT_PATIENT + "').to(g.V('" + target.ID + "'))";
            query += ".property('id','" + edgeID + "')";
            query += ".property('createdDate','" + edgeTimeStamp + "')";

            foreach (var keyValuePair in properties)
            {
                query += ".property('" + keyValuePair.Key + "','" + keyValuePair.Value + "')";
            }

            return query;
        }

        public string AddEdgeToHospital(Patient source, Hospital target, DateTime edgeTimeStamp, Dictionary<string, string> properties)
        {
            string edgeID = source.ID + "-" + target.ID;

            string query = "g.V('" + source.ID + "').addE('" + EDGE_PATIENT_HOSPITAL + "').to(g.V('" + target.ID + "'))";
            query += ".property('id','" + edgeID + "')";
            query += ".property('createdDate','" + edgeTimeStamp + "')";

            foreach (var keyValuePair in properties)
            {
                query += ".property('" + keyValuePair.Key + "','" + keyValuePair.Value + "')";
            }

            return query;
        }
    }
}
