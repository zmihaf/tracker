﻿using KawalCovid.Graph.FuncApp.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace KawalCovid.Graph.FuncApp.Service
{
    public class EventService : GraphServiceBase
    {
        public override string VertexLabel => "event";

        protected override string GetAddVertexQuery(IGraphVertex vertex)
        {
            Event eventVertex = vertex as Event;

            string queryAdd = "addV('" + VertexLabel + "').property('id', '" + eventVertex.ID + "')";
            queryAdd += ".property('pk', '" + eventVertex.ID + "')";
            queryAdd += ".property('city', '" + eventVertex.City + "')";
            queryAdd += ".property('createdDate', '" + eventVertex.CreatedDate + "')";
            queryAdd += ".property('name', '" + eventVertex.Name + "')";
            queryAdd += ".property('province', '" + eventVertex.Province + "')";
            queryAdd += ".property('place', '" + eventVertex.Place + "')";
            queryAdd += ".property('placeId', '" + eventVertex.PlaceID + "')";
            queryAdd += ".property('eventDate', '" + eventVertex.EventDate + "')";
            queryAdd += ".property('tag', '" + eventVertex.Tag + "')";

            return queryAdd;
        }
    }
}
