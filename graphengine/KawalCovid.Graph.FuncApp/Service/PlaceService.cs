﻿using KawalCovid.Graph.FuncApp.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace KawalCovid.Graph.FuncApp.Service
{
    public class PlaceService : GraphServiceBase
    {
        public override string VertexLabel => "place";

        protected override string GetAddVertexQuery(IGraphVertex vertex)
        {
            Place placeVertex = vertex as Place;

            string queryAdd = "addV('" + VertexLabel + "').property('id', '" + placeVertex.ID + "')";
            queryAdd += ".property('pk', '" + placeVertex.ID + "')";
            queryAdd += ".property('createdDate', '" + placeVertex.CreatedDate + "')";
            queryAdd += ".property('geofence', '" + placeVertex.Geofence + "')";
            queryAdd += ".property('latitude', '" + placeVertex.Latitude + "')";
            queryAdd += ".property('longitude', '" + placeVertex.Longitude + "')";
            queryAdd += ".property('name', '" + placeVertex.Name + "')";
            queryAdd += ".property('tag', '" + placeVertex.Tag + "')";

            return queryAdd;
        }
    }
}
