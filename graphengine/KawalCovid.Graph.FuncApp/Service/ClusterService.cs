﻿using KawalCovid.Graph.FuncApp.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace KawalCovid.Graph.FuncApp.Service
{
    public class ClusterService : GraphServiceBase
    {
        public const string EDGE_CLUSTER_PATIENT = "transmitted_from";

        public override string VertexLabel => "cluster";

        protected override string GetAddVertexQuery(IGraphVertex vertex)
        {
            Cluster clusterVertex = vertex as Cluster;

            string queryAdd = "addV('" + VertexLabel + "').property('id', '" + clusterVertex.ID + "')";
            queryAdd += ".property('pk', '" + clusterVertex.ID + "')";
            queryAdd += ".property('city', '" + clusterVertex.City + "')";
            queryAdd += ".property('createdDate', '" + clusterVertex.CreatedDate + "')";
            queryAdd += ".property('name', '" + clusterVertex.Name + "')";
            queryAdd += ".property('province', '" + clusterVertex.Province + "')";

            return queryAdd;
        }

        public string AddEdgeToPatient(Cluster source, Patient target, DateTime edgeTimeStamp, Dictionary<string, string> properties)
        {
            string edgeID = source.ID + "-" + target.ID;

            string query = "g.V('" + source.ID + "').addE('" + EDGE_CLUSTER_PATIENT + "').to(g.V('" + target.ID + "'))";
            query += ".property('id','" + edgeID + "')";
            query += ".property('createdDate','" + edgeTimeStamp + "')";

            foreach (var keyValuePair in properties)
            {
                query += ".property('" + keyValuePair.Key + "','" + keyValuePair.Value + "')";
            }

            return query;
        }
    }
}
