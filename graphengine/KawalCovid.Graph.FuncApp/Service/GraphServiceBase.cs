﻿using Gremlin.Net.Driver;
using KawalCovid.Graph.FuncApp.DataAccess;
using KawalCovid.Graph.FuncApp.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KawalCovid.Graph.FuncApp.Service
{
    public abstract class GraphServiceBase
    {
        public GraphDataAccess GraphDataAccess { get; set; }

        public abstract string VertexLabel { get; }

        public bool IsVertexExist(GremlinClient gremlinClient, string vertexId)
        {
            bool isExist = false;
            string queryCheck = "g.V('" + vertexId + "').count()";
            var resultSet = GraphDataAccess.SubmitRequest(gremlinClient, queryCheck).Result;

            if (resultSet.Count > 0)
            {
                foreach (var result in resultSet)
                {
                    string output = JsonConvert.SerializeObject(result);
                    isExist = output != "0";
                }
            }

            return isExist;
        }

        public bool IsEdgeExist(GremlinClient gremlinClient, string vertexId)
        {
            bool isExist = false;
            string queryCheck = "g.E('" + vertexId + "').count()";
            var resultSet = GraphDataAccess.SubmitRequest(gremlinClient, queryCheck).Result;

            if (resultSet.Count > 0)
            {
                foreach (var result in resultSet)
                {
                    string output = JsonConvert.SerializeObject(result);
                    isExist = output != "0";
                }
            }

            return isExist;
        }

        public virtual Task<ResultSet<dynamic>> AddVertex(GremlinClient client, IGraphVertex vertex)
        {
            if (vertex == null) { throw new ArgumentNullException("vertex cannot be null"); }

            //check if exists
            //g.V().has('<label>', 'id', '<id_value>').
            //  fold().
            //  coalesce(unfold(),
            //           addV('<label>').property('id', '<id_value>'))

            string addVertexQuery = this.GetAddVertexQuery(vertex);

            string gremlinQuery = "g.V().has('" + this.VertexLabel + "', 'id', '" + vertex.ID + "').";
            gremlinQuery += "fold().";
            gremlinQuery += "coalesce(unfold(),";
            gremlinQuery += addVertexQuery;
            gremlinQuery += ")";

            return this.GraphDataAccess.SubmitRequest(client, gremlinQuery);
        }

        public virtual Task<ResultSet<dynamic>> AddEdge(GremlinClient client, string gremlinQuery)
        {
            return this.GraphDataAccess.SubmitRequest(client, gremlinQuery);
        }

        protected abstract string GetAddVertexQuery(IGraphVertex vertex);


    }
}
