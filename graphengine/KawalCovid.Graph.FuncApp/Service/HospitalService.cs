﻿using KawalCovid.Graph.FuncApp.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace KawalCovid.Graph.FuncApp.Service
{
    public class HospitalService : GraphServiceBase
    {
        public override string VertexLabel => "hospital";

        protected override string GetAddVertexQuery(IGraphVertex vertex)
        {
            Hospital hospitalVertex = vertex as Hospital;

            string queryAdd = "addV('" + VertexLabel + "').property('id', '" + hospitalVertex.ID + "')";
            queryAdd += ".property('pk', '" + hospitalVertex.ID + "')";
            queryAdd += ".property('createdDate', '" + hospitalVertex.CreatedDate + "')";
            queryAdd += ".property('geofence', '" + hospitalVertex.Geofence + "')";
            queryAdd += ".property('latitude', '" + hospitalVertex.Latitude + "')";
            queryAdd += ".property('longitude', '" + hospitalVertex.Longitude + "')";
            queryAdd += ".property('name', '" + hospitalVertex.Name + "')";

            return queryAdd;
        }
    }
}
