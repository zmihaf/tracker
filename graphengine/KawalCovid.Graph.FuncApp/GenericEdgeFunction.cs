using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using KawalCovid.Graph.FuncApp.Service;
using KawalCovid.Graph.FuncApp.Model;
using System.Collections.Generic;

namespace KawalCovid.Graph.FuncApp
{
    public static class GenericEdgeFunction
    {
        [FunctionName("GenericEdgeFunction")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            try
            {
                string sourceID = String.Empty;
                string edgeLabel = String.Empty;
                string targetID = String.Empty;
                string sourceVertexType = String.Empty;

                string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
                dynamic data = JsonConvert.DeserializeObject(requestBody);

                //extract request body
                sourceVertexType = sourceVertexType ?? data?.sourceVertexType;
                sourceID = sourceID ?? data?.sourceId;
                edgeLabel = edgeLabel ?? data?.edgeLabel;
                targetID = targetID ?? data?.targetId;

                //instantiate service
                GraphServiceBase svc;
                switch (sourceVertexType)
                {
                    case "person":
                        svc = new PersonService();
                        break;

                    case "patient":
                        svc = new PatientService();
                        break;

                    case "cluster":
                        svc = new ClusterService();
                        break;

                    default:
                        throw new InvalidOperationException("sourceVertexType should be either 'person', 'patient' or 'cluster'");
                }
                svc.GraphDataAccess = new DataAccess.GraphDataAccess(log);

                //ingest edge information to CosmosDB
                using (var gremlinClient = svc.GraphDataAccess.InstantiateGremlinClient())
                {
                    string gremlinQuery = String.Empty;

                    switch (sourceVertexType)
                    {
                        case "person":

                            if (edgeLabel == PersonService.EDGE_PERSON_PLACE)
                            {
                                gremlinQuery = ((PersonService)svc).AddEdgeToPlace(new Person() { ID = sourceID }, new Place() { ID = targetID }, DateTime.Now, GetDictionaryObject(data));
                            }
                            else if (edgeLabel == PersonService.EDGE_PERSON_PERSON)
                            {
                                gremlinQuery = ((PersonService)svc).AddEdgeToPerson(new Person() { ID = sourceID }, new Person() { ID = targetID }, DateTime.Now, GetDictionaryObject(data));
                            }
                            else if (edgeLabel == PersonService.EDGE_PERSON_EVENT)
                            {
                                gremlinQuery = ((PersonService)svc).AddEdgeToEvent(new Person() { ID = sourceID }, new Event() { ID = targetID }, DateTime.Now, GetDictionaryObject(data));
                            }
                            else if (edgeLabel == PersonService.EDGE_PERSON_PATIENT)
                            {
                                gremlinQuery = ((PersonService)svc).AddEdgeToPatient(new Person() { ID = sourceID }, new Patient() { ID = targetID }, DateTime.Now, GetDictionaryObject(data));
                            }

                            break;

                        case "patient":

                            if (edgeLabel == PatientService.EDGE_PATIENT_HOSPITAL)
                            {
                                gremlinQuery = ((PatientService)svc).AddEdgeToHospital(new Patient() { ID = sourceID }, new Hospital() { ID = targetID }, DateTime.Now, GetDictionaryObject(data));
                            }
                            else if (edgeLabel == PatientService.EDGE_PATIENT_PATIENT)
                            {
                                gremlinQuery = ((PatientService)svc).AddEdgeToPatient(new Patient() { ID = sourceID }, new Patient() { ID = targetID }, DateTime.Now, GetDictionaryObject(data));
                            }

                            break;

                        case "cluster":
                            if (edgeLabel == ClusterService.EDGE_CLUSTER_PATIENT)
                            {
                                gremlinQuery = ((ClusterService)svc).AddEdgeToPatient(new Cluster() { ID = sourceID }, new Patient() { ID = targetID }, DateTime.Now, GetDictionaryObject(data));
                            }

                            break;

                    }

                    if (String.IsNullOrEmpty(gremlinQuery))
                    {
                        throw new InvalidOperationException("Invalid input: " + requestBody);
                    }
                    else
                    {
                        //ingest to CosmosDB
                        await svc.AddEdge(gremlinClient, gremlinQuery);
                    }
                }

                return (ActionResult)new OkObjectResult($"{data.ID}");
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Bad Request: " + e.Message);
            }
        }

        private static Dictionary<string, string> GetDictionaryObject(dynamic data)
        {
            string edgeProperties = String.Empty;
            Dictionary<string, string> result = new Dictionary<string, string>();
            try
            {
                edgeProperties = edgeProperties ?? data?.edgeProperties;

                //e.g. "placeID|<placeIDValue>,event|<eventIDValue>,tag|<tagValue>"
                string[] properties = edgeProperties.Split(new char[] { ',' });
                foreach (string property in properties)
                {
                    string[] keyValue = property.Split(new char[] { '|' });
                    if (!result.ContainsKey(keyValue[0]))
                    {
                        result.Add(keyValue[0], keyValue[1]);
                    }
                }
            }
            catch (Exception)
            {
                //ignore exception
            }

            return result;
        }
    }
}
