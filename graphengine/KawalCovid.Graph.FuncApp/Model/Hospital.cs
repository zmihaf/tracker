﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace KawalCovid.Graph.FuncApp.Model
{
    public class Hospital : IGraphVertex
    {
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "geofence")]
        public string Geofence { get; set; }

        [JsonProperty(PropertyName = "longitude")]
        public double Longitude { get; set; }

        [JsonProperty(PropertyName = "latitude")]
        public double Latitude { get; set; }

        [JsonProperty(PropertyName = "createdDate")]
        public DateTime CreatedDate { get; set; }


    }
}
