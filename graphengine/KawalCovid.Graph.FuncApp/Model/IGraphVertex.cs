﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KawalCovid.Graph.FuncApp.Model
{
    public interface IGraphVertex
    {
        string ID { get; set; }

        DateTime CreatedDate { get; set; }
    }
}
